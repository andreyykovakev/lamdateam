package com.lamda.dailytask05_04;

import java.util.ArrayList;

public class Demo2 {

    public static void main(String[] args) {
        Object[] objects = {"one", "two", "three"};
        String result = StringUtil.getStringFromObjectsArray(objects, "-");
        System.out.println(result);

        ArrayList arr = new ArrayList();
        arr.add("lo");
        arr.add("lo");
        arr.add(objects);
        System.out.println(StringUtil.getStringFromObjectsArray(arr, "!!"));
    }
}
