package com.lamda.dailytask05_04;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class StringUtil {


    public static String getStringFromObjectsArray(Object[] objects, Object delimeter){

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < objects.length - 1; i++) {
            result.append(objects[i]).append(" " + delimeter + " ");
        }
        result.append(objects[objects.length - 1]);

        return result.toString();
    }

    public static String getStringFromObjectsArray(Iterable objects, Object delimeter) {

        StringBuilder result = new StringBuilder();

        Iterator iterator = objects.iterator();
        while (iterator.hasNext()) {
            result.append(iterator.next());
            if (iterator.hasNext()) {
                result.append(" " + delimeter + " ");
            }
        }

        return result.toString();
    }
}
