package com.lamda.task1;

import com.lamda.task1.entity.User;
import com.lamda.task1.model.ModelUser;
import com.lamda.task1.retrofit.task2.Models;
import com.lamda.task1.retrofit.task2.SiteDataProvider;
import com.lamda.task1.retrofit.task3.SiteDataProvider2;
import com.lamda.task1.retrofit.task3.entities.CarDetails;
import com.lamda.task1.retrofit.task3.entities.Info;
import com.lamda.task1.utils.*;

import java.io.*;

public class Demo {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ModelUser mu = new ModelUser();
        HttpRequestHandler httpHandler = new HttpRequestHandler();
        OkioHandler okio = new OkioHandler();
        OkHttpRequestHandler okhttp = new OkHttpRequestHandler();
        SiteDataProvider dataProvider = new SiteDataProvider();
        SiteDataProvider2 dataProvider2 = new SiteDataProvider2();
        Models carModels;
        CarDetails modelDetails;
        String result;

        String userInput = "";
        while (!userInput.equals("exit")) {
            userInput = reader.readLine();
            switch (userInput) {
                case "2":
                    System.out.println("hello from task2 2");
                    break;
                case "3":
                    mu.createUser();
                    System.out.println(mu.getLastUser());
                    break;
                case "4":
                    mu.printUserByEmail(reader.readLine());
                    break;
                case "5":
                    mu.printUserInJSON(reader.readLine());
                    break;
                case "6":
                    User userTask6 = mu.createUser();
                    System.out.println(userTask6);
                    FileUtils.writeUserToFile(userTask6);
                    break;
                case "7":
                    FileUtils.printUsersFromFile();
                    break;
                case "8":
                    //TODO from ObjectMapperToCollection - DONE
                    User userTask8 = mu.createUser();
                    System.out.println(FileUtils.serializeUsers(userTask8));
                    break;
                case "9":
                    System.out.println(FileUtils.deserializeUsers().toString());
                    break;
                case "10":
                    StringUtils.printingWordsFromSplitString(reader.readLine());
                    break;
                case "11":
                    StringUtils.printReversedString(reader.readLine());
                    break;
                case "12":
                    StringUtils.splitStringAndPrintWordsFrequency(reader.readLine());
                    break;
                case "13":
                    System.out.println(StringUtils.splitStringAndGroupByWordLength(reader.readLine()));
                    break;
                case "14":
                    System.out.println(StringUtils.stringToUniqueWordsGroupedByWordLength(reader.readLine()));
                    break;
                case "15":
                    String responseBody = httpHandler.getResponseBodyOfRequestMethodGet("https://httpbin.org/get", "param");
                    System.out.print(responseBody);
                    break;
                case "16":
                    String responseBodyPostMethod = httpHandler.getResponseBodyOfRequestMethodPost("https://httpbin.org/post", "param");
                    System.out.println(responseBodyPostMethod);
                    break;
                case "17":
                    HttpRequestHandler.printUrlHeadersInfo("https://httpbin.org/headers");
                    break;
                case "18read":
                    System.out.println("Enter path to a file: ");
                    result = okio.readFile(new File(reader.readLine()));
                    System.out.println(result);
                    break;
                case "18write":
                    okio.writeDataToFile("okiotest.txt", "writing now");
                    break;
                case "18copy":
                    okio.copyFromFileToFile("test.txt", "okiotest.txt");
                    break;
                case "19.1":
                    result = okhttp.getResponseMethodGet("https://httpbin.org/get");
                    System.out.println(result);
                    break;
                case "19.2":
                    result = okhttp.getResponseMethodPost("https://httpbin.org/post");
                    System.out.println(result);
                    break;
                case "19.3":
                    result = okhttp.getResponseMethodGet("https://httpbin.org/headers");
                    System.out.println(result);
                    break;
                case "20.2":
                    carModels = dataProvider.getModelsByDealerId(693015);
                    if(carModels != null){
                        System.out.println( "Dealer: " + carModels.getMaker().getName());
                        for (int i = 0; i < carModels.getModels().size(); i++) {
                            System.out.println(carModels.getModels().get(i));
                        }
                    }
                    break;
                case "20.3":
                    modelDetails = dataProvider2.getModelsByDealerId(693015,247841);
                    if(modelDetails != null){
                        System.out.println( "Model" + modelDetails.getName());
                        System.out.println(modelDetails.getInfo());
                    }
                    break;
                default:
                    System.out.println(userInput);
                    break;
            }
        }
    }
}
