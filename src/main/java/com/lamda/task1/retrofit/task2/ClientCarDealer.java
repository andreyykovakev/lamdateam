package com.lamda.task1.retrofit.task2;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface ClientCarDealer {

    String BASE_URL = "https://api.ukrdisk.com/";

    @GET("makers/{id}")
    Call<Models> getCarDealers(@Path("id") int id);
}