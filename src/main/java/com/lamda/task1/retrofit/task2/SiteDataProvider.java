package com.lamda.task1.retrofit.task2;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

public class SiteDataProvider {
    public static void main(String[] args) {
        Models models = null;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ClientCarDealer.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        ClientCarDealer api = retrofit.create(ClientCarDealer.class);
        System.out.println();

        try {
            models = api.getCarDealers(693008).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Models getModelsByDealerId(int id){
        Models models = null;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ClientCarDealer.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        ClientCarDealer api = retrofit.create(ClientCarDealer.class);
        System.out.println();

        try {
            models = api.getCarDealers(id).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return models;
    }
}
