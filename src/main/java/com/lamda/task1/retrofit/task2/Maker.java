package com.lamda.task1.retrofit.task2;


public class Maker {
    private String name;
    private int id;

    public Maker(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }



    @Override
    public String toString() {
        return "Maker{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}
