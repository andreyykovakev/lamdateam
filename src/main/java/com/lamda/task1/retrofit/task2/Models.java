package com.lamda.task1.retrofit.task2;

import java.util.List;

public class Models {
    private int id;
    private String name;
    private Maker maker;
    private List<Models> models;

    @Override
    public String toString() {
        return "Models{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Maker getMaker() {
        return maker;
    }

    public List<Models> getModels() {
        return models;
    }

    public Models() {

    }

    public Models(int id, String name, Maker maker, List<Models> models) {

        this.id = id;
        this.name = name;
        this.maker = maker;
        this.models = models;
    }





}