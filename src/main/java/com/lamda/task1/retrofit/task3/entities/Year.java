
package com.lamda.task1.retrofit.task3.entities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Year {

    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("configs")
    @Expose
    private List<Config> configs = null;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public List<Config> getConfigs() {
        return configs;
    }

    public void setConfigs(List<Config> configs) {
        this.configs = configs;
    }

    @Override
    public String toString() {
        return "Year{" +
                "value=" + value +
                ", configs=" + configs +
                '}';
    }
}
