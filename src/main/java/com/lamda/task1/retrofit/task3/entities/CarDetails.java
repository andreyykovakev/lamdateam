
package com.lamda.task1.retrofit.task3.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarDetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("maker")
    @Expose
    private Maker maker;
    @SerializedName("info")
    @Expose
    private Info info;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Maker getMaker() {
        return maker;
    }

    public void setMaker(Maker maker) {
        this.maker = maker;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "CarDetails{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", maker=" + maker +
                ", info=" + info +
                '}';
    }
}
