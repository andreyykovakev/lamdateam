
package com.lamda.task1.retrofit.task3.entities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Config {

    @SerializedName("pcd")
    @Expose
    private String pcd;
    @SerializedName("rims")
    @Expose
    private List<Rim> rims = null;
    @SerializedName("engines")
    @Expose
    private List<String> engines = null;

    public String getPcd() {
        return pcd;
    }

    public void setPcd(String pcd) {
        this.pcd = pcd;
    }

    public List<Rim> getRims() {
        return rims;
    }

    public void setRims(List<Rim> rims) {
        this.rims = rims;
    }

    public List<String> getEngines() {
        return engines;
    }

    public void setEngines(List<String> engines) {
        this.engines = engines;
    }

    @Override
    public String toString() {
        return "Config{" +
                "pcd='" + pcd + '\'' +
                ", rims=" + rims +
                ", engines=" + engines +
                '}';
    }
}
