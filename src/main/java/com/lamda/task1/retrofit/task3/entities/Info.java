
package com.lamda.task1.retrofit.task3.entities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Info {
    @Override
    public String toString() {
        return "Info{" +
                "years=" + years +
                '}';
    }

    @SerializedName("years")
    @Expose
    private List<Year> years = null;

    public List<Year> getYears() {
        return years;
    }

    public void setYears(List<Year> years) {
        this.years = years;
    }

}
