package com.lamda.task1.retrofit.task3;


import com.lamda.task1.retrofit.task3.entities.CarDetails;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;

public class SiteDataProvider2 {

    public static void main(String[] args) {
        CarDetails models = null;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ClientCarDealer.BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        ClientCarDealer api = retrofit.create(ClientCarDealer.class);

        try {
            models = api.getCarDealers(693008, 247738).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(models.getInfo());
    }

    public CarDetails getModelsByDealerId(int id_dealer, int id_model){
        CarDetails models = null;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ClientCarDealer.BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        ClientCarDealer api = retrofit.create(ClientCarDealer.class);

        try {
            models = api.getCarDealers(id_dealer, id_model).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return models;
    }
}
