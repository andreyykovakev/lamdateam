package com.lamda.task1.retrofit.task3;

import com.lamda.task1.retrofit.task3.entities.CarDetails;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface ClientCarDealer {

    String BASE_URL = "https://api.ukrdisk.com/";

    @GET("makers/{makers_id}/models/{models_id}")
    Call<CarDetails> getCarDealers(@Path("makers_id") int idDealer, @Path("models_id") int idCar);
}