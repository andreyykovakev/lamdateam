
package com.lamda.task1.retrofit.task3.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rim {

    @SerializedName("diameter")
    @Expose
    private String diameter;
    @SerializedName("width")
    @Expose
    private String width;

    public String getDiameter() {
        return diameter;
    }

    public void setDiameter(String diameter) {
        this.diameter = diameter;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    @Override
    public String toString() {
        return "Rim{" +
                "diameter='" + diameter + '\'' +
                ", width='" + width + '\'' +
                '}';
    }
}
