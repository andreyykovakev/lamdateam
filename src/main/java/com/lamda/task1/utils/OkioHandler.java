package com.lamda.task1.utils;

import okio.*;

import java.io.*;
import java.nio.charset.Charset;

public class OkioHandler {

    public void writeDataToFile(String path, Object data) throws IOException {
        File file = new File(path);

        BufferedSink sink = Okio.buffer(Okio.appendingSink(file)); //to get an access to a file
        sink.write(data.toString().getBytes()); //writing data to a file
        sink.close();
    }

    public void writeDataToFile(String path, String string) throws IOException {
        File file = new File(path);

        BufferedSink sink = Okio.buffer(Okio.appendingSink(file));
        sink.writeString(string, Charset.defaultCharset());
        sink.close();
    }

    public void writeDataToFile(String path, int number) throws IOException {
        File file = new File(path);

        BufferedSink sink = Okio.buffer(Okio.appendingSink(file));
        sink.writeInt(number);
        sink.close();
    }

    public void copyFromFileToFile(String copyFrom, String copyTo) {
        try (Source a = Okio.source(new File(copyFrom)); BufferedSink b = Okio.buffer(Okio.sink(new File(copyTo)))) {
            b.writeAll(a);
            System.out.println("File has been copied successfully!");
        } catch (IOException e) {
            System.err.println("Opps, something went wrong!");
        }
    }

    public String readFile(File file) throws IOException {
        String result;
        BufferedSource source = Okio.buffer(Okio.source(file)); //get an access to a file to reading
        result = source.readUtf8(); //reading from a file
        source.close();
        return result;
    }
}

