package com.lamda.task1.utils;

import com.lamda.task1.entity.User;
import com.sun.xml.internal.ws.developer.SerializationFeature;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.type.CollectionType;
import org.codehaus.jackson.util.DefaultPrettyPrinter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileUtils {
    public static void writeUserToFile(User user) {
        try (FileWriter writer = new FileWriter("users.txt", true)) {
            writer.append(user.toString());
            writer.append('\n');
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void printUsersFromFile() {
        try (FileReader fr = new FileReader("users.txt")) {
            int c;
            while ((c = fr.read()) != -1) {
                System.out.print((char) c);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static String serializeUsers(User user) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        CollectionType javaType = mapper.getTypeFactory()
                .constructCollectionType(List.class, User.class);

        List<User> users = mapper.readValue(new File("users_serialized.json"), javaType);

        users.add(user);

        ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
        writer.writeValue(new File("users_serialized.json"), users);

        String jsonInString = null;

        try {
            jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonInString;
    }

    public static List<User> deserializeUsers() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        CollectionType javaType = mapper.getTypeFactory()
                .constructCollectionType(List.class, User.class);

        List<User> users = mapper.readValue(new File("users_serialized.json"), javaType);
        return users;
    }
}
