package com.lamda.task1.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import java.net.URL;

public class HttpRequestHandler {
    public String getResponseBodyOfRequestMethodGet(String url, String params) {
        StringBuffer result;
        BufferedReader in = null;
        StringBuffer response = null;
        int responseCode = 0;

        try {
            URL obj = new URL(url + "?" + params);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("GET");

            responseCode = con.getResponseCode();

            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        } catch (IOException e) {
            System.err.print("Couldn't get a response from the server");
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                System.err.println("Error while trying to close connection");
            }
        }

        if (response != null) {
            result = new StringBuffer("Response Body : " + "'GET' request to URL : " + url + "\n").append("Response Code : ").append(responseCode).append("\n").append(response);
            return result.toString();
        } else {
            return "";
        }
    }

    public String getResponseBodyOfRequestMethodPost(String url, String params) {
        StringBuffer result;
        BufferedReader in = null;
        StringBuffer response = null;
        int responseCode = 0;

        try {

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            con.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());

            wr.writeBytes(params);
            wr.flush();
            wr.close();

            responseCode = con.getResponseCode();

            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;

            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    System.err.println("Error while trying to close connection");
                }
            }
        }
        if (response != null) {
            result = new StringBuffer("Response Body : " + "'POST' request to URL : " + url + "\n").append("Response Code : ").append(responseCode).append("\n").append(response);
            return result.toString();
        } else {
            return "";
        }
    }
    public static void printUrlHeadersInfo(String str) throws IOException {
        URL url = new URL(str);

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
            for (String line; (line = reader.readLine()) != null;) {
                System.out.println(line);
            }
        }
    }
}
