package com.lamda.task1.utils;

import okhttp3.*;

import java.io.IOException;

public class OkHttpRequestHandler {


    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    OkHttpClient client = new OkHttpClient();

    public String getResponseMethodGet(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    public String getResponseMethodPost(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();
            String param = "{\n" +
                    "  \"name\" : \"qwe\",\n" +
                    "  \"email\" : \"qwe\",\n" +
                    "  \"age\" : 1\n" +
                    "}";

            RequestBody body = RequestBody.create(JSON, param);
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }


    public String getRequestHeaders(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            try (Response response = client.newCall(request).execute()) {

                Headers responseHeaders = response.headers();
                for (int i = 0; i < responseHeaders.size(); i++) {
                    System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
                }

                return response.body().string();
            }
    }

}
