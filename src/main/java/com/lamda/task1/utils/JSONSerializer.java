package com.lamda.task1.utils;

import com.lamda.task1.entity.User;
import org.codehaus.jackson.map.ObjectMapper;


import java.io.IOException;


public class JSONSerializer {

    public static void printSerializedUser(User user) throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        mapper.writeValue(System.out, user);

        String jsonInString = null;

        try {
            jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(jsonInString);
    }
}
