package com.lamda.task1.utils;

import java.util.*;

public class StringUtils {

    public static void splitStringAndPrintWordsFrequency(String str) {
        Map<Integer, Integer> listOfWords = new HashMap<>();

        for (String retval : str.split(" ")) {
            int newValue = listOfWords.getOrDefault(retval.length(), 0) + 1;
            listOfWords.put(retval.length(), newValue);
        }
        System.out.println(listOfWords.toString());
    }

    public static void printReversedString(String stringToReverse) {

        for (int i = stringToReverse.length() - 1; i >= 0; i--) {

            System.out.print(stringToReverse.charAt(i));

        }
    }

    public static void printingWordsFromSplitString(String entireString) {
        String[] arr = entireString.split(" ");

        for (int i = 0; i < arr.length - 1; i++) {
            System.out.print(arr[i] + ", ");
        }
        System.out.print(arr[arr.length - 1]);
    }

    public static Map splitStringAndGroupByWordLength(String str){
        Map<Integer,List<String>> map = new HashMap<>();
        for (String word:str.split(" ")) {
            int length = word.length();
            if(!map.containsKey(length)){
                map.put(length, new ArrayList<>());
            }
            map.get(length).add(word);

        }
        return map;
    }

    public static Map stringToUniqueWordsGroupedByWordLength(String str){
        Map<Integer,Set<String>> map = new HashMap<>();
        for (String word:str.split(" ")) {
            int length = word.length();
            if(!map.containsKey(length)){
                map.put(length, new HashSet<>());
            }
            map.get(length).add(word);

        }
        return map;
    }
}
