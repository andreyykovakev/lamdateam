package com.lamda.task1.model;

import com.lamda.task1.utils.JSONSerializer;
import com.lamda.task1.entity.User;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ModelUser {

    private List<User> users = new ArrayList<>();

    public List<User> getUsers() {
        return users;
    }

    public User getLastUser(){
        return users.get(users.size() - 1);

    }

    public User createUser() {

        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
        String name = null;
        String email = null;
        int age = 0;

        try {
            System.out.println("Enter user name:");
            name = consoleReader.readLine();
            System.out.println("Enter user email:");
            email = consoleReader.readLine();
            System.out.println("Enter user age:");
            age = Integer.parseInt(consoleReader.readLine());

        } catch (IOException e) {
            //gotta add logger here later
        }
        User user = new User(name, email, age);
        users.add(user);

        return user;
    }

    public void printUserByEmail(String email){

        if (email.equals("")) {
            Random rn = new Random();
            int range = users.size();
            int randomUserIndex = rn.nextInt(range);

            System.out.println(users.get(randomUserIndex));

        } else {
            for (int i = 0; i < users.size(); i++) {
                User user1 = users.get(i);
                if (user1.getEmail().equals(email)) {
                    System.out.println(user1);
                }
            }
        }
    }
    public void printUserInJSON(String email) throws IOException {

        if (email.equals("")) {

            Random rn = new Random();
            int range = users.size();
            int randomUserIndex = rn.nextInt(range);

            ObjectMapper mapper = new ObjectMapper();

            String jsonInString = null;

            try {
                jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(users.get(randomUserIndex));
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(jsonInString);

        } else {
            for (int i = 0; i < users.size(); i++) {
                User user1 = users.get(i);
                if (user1.getEmail().equals(email)) {
                    JSONSerializer.printSerializedUser(user1);
                }
            }
        }
    }
}
